﻿using System.Web.Http;

namespace LunchParse
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "LunchApi",
                routeTemplate: "lunch/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "Help" }
            );
        }
    }
}
