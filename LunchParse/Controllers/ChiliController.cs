﻿using System;
using System.Web.Http;
using LunchParse.Libraries;

namespace LunchParse.Controllers
{
    public class ChiliController : ApiController
    {
        public string Get()
        {
            try
            {
                var chiliParser = new ChiliParser();

                if (!chiliParser.HasMenuToday())
                {
                    return "There is no menu today.";
                }

                return chiliParser.GetTodaysMenu();
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message} - {ex.StackTrace}";
            }
        }
    }
}
