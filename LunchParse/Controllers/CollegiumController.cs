﻿using System;
using System.Web.Http;
using LunchParse.Libraries;

namespace LunchParse.Controllers
{
    public class CollegiumController : ApiController
    {
        public string Get()
        {
            try
            {
                var collegiumParser = new CollegiumParser();

                if (!collegiumParser.HasMenuToday())
                {
                    return "There is no menu today.";
                }

                return collegiumParser.GetTodaysMenu();
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message} - {ex.StackTrace}";
            }
        }
    }
}
