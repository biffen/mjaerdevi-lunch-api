﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using LunchParse.Interfaces;
using LunchParse.Libraries;
using LunchParse.Models;
using NLog;

namespace LunchParse.Controllers
{
    public class HelpController : ApiController
    {
        public string Get()
        {
            var logger = LogManager.GetLogger("Lunch-Api");
            logger.Debug("Testing get");
            return "Choose either /husman, /chili or /collegium to view todays menu.";
        }

        public HttpResponseMessage Post(SlackMessageModel model)
        {
            var logger = LogManager.GetLogger("Lunch-Api");
            try
            {
                logger.Debug($"Text: {model?.text ?? "empty"}. Channel: {model?.channel_name ?? "empty"}");

                var message = model?.text?.Trim().ToLower() ?? string.Empty;

                var parsers = new List<IFoodParser>();
                if (message.Contains("husman"))
                {
                    parsers.Add(new HusmanParser());
                }
                else if (message.Contains("chili"))
                {
                    parsers.Add(new ChiliParser());
                }
                else if (message.Contains("collegium"))
                {
                    parsers.Add(new CollegiumParser());
                }
                else
                {
                    parsers.Add(new HusmanParser());
                    parsers.Add(new ChiliParser());
                    parsers.Add(new CollegiumParser());
                }

                var returnMsg = string.Empty;
                foreach (var parser in parsers)
                {
                    try
                    {
                        if (!parser.HasMenuToday())
                        {
                            returnMsg += $"<br />There is no menu for {parser.Name} today. <br />";
                        }

                        var menuText = parser.GetTodaysMenu();
                        if (parser.Name == "Chili")
                        {
                            var regexOne = new Regex("(\\s{2,})", RegexOptions.None);
                            var regexTwo = new Regex("\\d{1,2}\\.(<br>|<br />|$)", RegexOptions.None);
                            menuText = regexOne.Replace(menuText, "");

                            //logger.Debug("First pass: " + menuText);

                            menuText = regexTwo.Replace(menuText, "");

                            //logger.Debug("Second pass: " + menuText);
                        }

                        returnMsg += $"<br />*{parser.Name} menu* <br /> {menuText} <br />";
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex, $"Unable to get menu for {parser.Name} right now.");
                        returnMsg += $"<br />Unable to get menu for {parser.Name} right now. Sorry about that :( <br />";
                    }
                }

                //logger.Debug("Complete return message: " + returnMsg);

                returnMsg = returnMsg
                    .Replace("\r\n", string.Empty)
                    .Replace("\n", string.Empty)
                    .Replace("\r", string.Empty)
                    .Replace("<br />", " \n")
                    .Replace("<br>", " \n");

                //logger.Debug("Cleaned return message: " + returnMsg);

                Task.Run(() => SendSlackMessageTask(returnMsg, model?.channel_name ?? "#test-integrations", logger));

                logger.Debug("Return ok");

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"Exception when running API method.");
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        private static async Task SendSlackMessageTask(string text, string channel, ILogger logger)
        {
            var urlWithAccessToken = "https://hooks.slack.com/services/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";

            var client = new SlackClient(urlWithAccessToken);

            logger.Debug("Posting answer back to slack.");

            client.PostMessage(username: "Mjärdevi Lunch App",
                text: text,
                channel: channel);

            logger.Debug("Posting answer back to slack is done.");
        }
    }
}
