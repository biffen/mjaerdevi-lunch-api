﻿using System;
using System.Web.Http;
using LunchParse.Libraries;

namespace LunchParse.Controllers
{
    public class HusmanController : ApiController
    {
        public string Get()
        {
            try
            {
                var husmanParser = new HusmanParser();

                if (!husmanParser.HasMenuToday())
                {
                    return "There is no menu today.";
                }

                return husmanParser.GetTodaysMenu();
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message} - {ex.StackTrace}";
            }
        }
    }
}
