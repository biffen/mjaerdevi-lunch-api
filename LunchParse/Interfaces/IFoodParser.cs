﻿namespace LunchParse.Interfaces
{
    public interface IFoodParser
    {
        string Name { get; }
        bool HasMenuToday();
        string GetTodaysMenu();
    }
}