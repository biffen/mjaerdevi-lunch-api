﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using HtmlAgilityPack;
using LunchParse.Interfaces;

namespace LunchParse.Libraries
{
    public class ChiliParser : IFoodParser
    {
        private const string Url = "http://www.chili-lime.se/helaveckan.asp";

        public string Name => "Chili";

        private readonly HtmlDocument _doc;

        public ChiliParser()
        {
            _doc = ParserHelper.GetHtmlDocument(Url, Encoding.GetEncoding(1252));
        }

        public bool HasMenuToday()
        {
            var chiliMenu = _doc.DocumentNode.SelectNodes("//span[@class='underRubrik']");

            if (chiliMenu == null)
                return false;

            var culture = new System.Globalization.CultureInfo("sv-SE");
            var today = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            var todaysMenu = chiliMenu.FirstOrDefault(m => m.InnerText.ToLower().Contains(today));

            if (todaysMenu == null)
                return false;

            return true;
        }

        public string GetTodaysMenu()
        {
            var chiliMenu = _doc.DocumentNode.SelectNodes("//span[@class='underRubrik']");

            var culture = new System.Globalization.CultureInfo("sv-SE");
            var today = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            var todaysMenu = chiliMenu.FirstOrDefault(m => HttpUtility.HtmlDecode(m.InnerText.ToLower()).Contains(today));

            var finishedMenu = todaysMenu.ParentNode.InnerHtml;
            finishedMenu = finishedMenu.Substring(finishedMenu.IndexOf("<br>", StringComparison.Ordinal)+4);

            return finishedMenu;
        }
    }
}