﻿using System;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using LunchParse.Interfaces;

namespace LunchParse.Libraries
{
    public class CollegiumParser : IFoodParser
    {
        private const string Url = "http://www.sodexomeetings.se/collegium/restaurang-och-cafe/lunchmeny/";

        public string Name => "Collegium";

        private readonly HtmlDocument _doc;

        private readonly string[] _weekDays = { "måndag", "tisdag", "onsdag", "torsdag", "fredag" };

        public CollegiumParser()
        {
            _doc = ParserHelper.GetHtmlDocument(Url);
        }

        public bool HasMenuToday()
        {
            var menuArea = _doc.DocumentNode.SelectNodes("//div[@class='span5 column_page column_right']//p");

            if (menuArea == null)
                return false;

            var culture = new System.Globalization.CultureInfo("sv-SE");
            var today = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            var todayNode = menuArea.FirstOrDefault(m => m.InnerText.ToLower().Contains(today));

            if (todayNode == null)
                return false;

            return true;
        }

        public string GetTodaysMenu()
        {
            var menuArea = _doc.DocumentNode.SelectNodes("//div[@class='span5 column_page column_right']//p");
            
            var culture = new System.Globalization.CultureInfo("sv-SE");
            var today = culture.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek);

            var todayNode = menuArea.FirstOrDefault(m => m.InnerText.ToLower().Contains(today));

            var counter = 1;
            var todaysMenuBuilder = new StringBuilder();
            while (!ContainsWeekDay(menuArea[menuArea.IndexOf(todayNode) + counter].InnerText.ToLower()))
            {
                todaysMenuBuilder.Append(menuArea[menuArea.IndexOf(todayNode) + counter].InnerText);
                todaysMenuBuilder.Append("<br />");
                counter++;
            }

            return todaysMenuBuilder.ToString();
        }

        private bool ContainsWeekDay(string text)
        {
            return _weekDays.Any(weekDay => text.ToLower().Contains(weekDay));
        }
    }
}