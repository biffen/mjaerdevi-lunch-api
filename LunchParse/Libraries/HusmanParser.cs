﻿using System.Linq;
using System.Net;
using System.Text;
using HtmlAgilityPack;
using LunchParse.Interfaces;

namespace LunchParse.Libraries
{
    public class HusmanParser : IFoodParser
    {
        private const string Url = "https://restauranghusman.se/";

        public string Name => "Husman";

        private readonly HtmlDocument _doc;

        public HusmanParser()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            _doc = ParserHelper.GetHtmlDocument(Url);
        }

        public bool HasMenuToday()
        {
            var noMenuToday = _doc.DocumentNode.SelectNodes("//div[@class='todays-menu']")?.FirstOrDefault();

            return noMenuToday != null && !noMenuToday.InnerText.Contains("Det finns ingen meny för idag.");
        }

        public string GetTodaysMenu()
        {
            var husmanMenu = _doc.DocumentNode.SelectNodes("//div[@class='todays-menu']//li");

            if (husmanMenu == null)
                return "Husman is null unfortunately";

            var menuBuilder = new StringBuilder();
            foreach (var node in husmanMenu)
            {
                menuBuilder.Append(node.InnerText);
                menuBuilder.Append("<br />");
            }

            return menuBuilder.ToString();
        }
    }
}