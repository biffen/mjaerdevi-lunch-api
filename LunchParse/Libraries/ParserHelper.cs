﻿using System.Runtime.Remoting.Messaging;
using System.Text;
using HtmlAgilityPack;

namespace LunchParse.Libraries
{
    public static class ParserHelper
    {
        public static HtmlDocument GetHtmlDocument(string url, Encoding enc = null)
        {
            var web = new HtmlWeb();

            if (enc != null) web.OverrideEncoding = enc;

            web.PreRequest += request =>
            {
                request.CookieContainer = new System.Net.CookieContainer();
                //request.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
                return true;
            };

            return web.Load(url, "GET");
        }
    }
}